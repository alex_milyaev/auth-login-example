export default {
   state: {
       user: localStorage.getItem('user') || ''
   },
   mutations: {
    SET_USER(state, payload) {
        state.user = payload;
    }
   },
   actions: {
    AUTH_REQUEST({ commit }, payload) {
        return new Promise((resolve, reject) => {
            axios.post('http://localhost:3000/users', payload)
            .then(response => {
                let user = response.data;
                resolve(user)
                commit('SET_USER', user)
            })
            .catch(error => {
                reject(error)
                console.log("Error", error)
            })
        })
    },
    LOGIN_REQUEST({ commit }, payload) {
        return new Promise((resolve, reject) => {
            axios.get('http://localhost:3000/users')
            .then(response => {
                let users = response.data;
                users.forEach(user => {
                    if(user.email === payload.email && user.password === payload.password) {
                        resolve(user)
                        commit('SET_USER', user)
                    }
                })
            })
            .catch(error => {
                reject(error)
                console.log("Error", error)
            })
        })
    }
   },
   getters: {
       GET_USER(state) {
           return state.user
       }
   } 
}