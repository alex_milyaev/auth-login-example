export default {
    state: {
        articles: [],
        search: '',
        sortDate: false
    },
    mutations: {
        SET_ARTICLES(state, payload) {
            state.articles = payload
        },
        SET_SEARCH(state, payload) {
            state.search = payload
        },
        SET_SORTDATE(state) {
            state.sortDate = !state.sortDate
        }
    },
    actions: {
        LOAD_ARTICLES({ commit }) {
            return new Promise((resolve, reject) => {
                axios.get('http://localhost:3000/articles')
                .then(response => {
                    resolve(response.data)
                    commit('SET_ARTICLES', response.data)
                })
                .catch(error => {
                    reject(error)
                    console.log("Error", error)
                })
            })
        }
    },
    getters: {
        GET_ARTICLES(state) {
            return state.articles
        },
        GET_SEARCH(state) {
            return state.search
        },
        GET_SORTDATE(state) {
            return state.sortDate
        }
    }
}