import Vue from 'vue'
import VueRouter from 'vue-router'
import store from "@/store"

Vue.use(VueRouter)

const routes = [
  {
    path: '*',
    meta: { layout: 'auth', auth: false },
    component: () =>
        import('../views/404.vue')
  },
  {
    path: '/auth',
    name: 'auth',
    meta: { layout: 'auth', auth: false },
    component: () =>
        import('../views/Auth.vue')
  },
  {
    path: '/login',
    name: 'Login',
    meta: { layout: 'auth', auth: false },
    component: () =>
        import('../views/Login.vue')
  },
  {
    path: '/',
    name: 'home',
    meta: { layout: 'default', auth: true },
    component: () => import('../views/Home.vue')
  },
  {
    path: '/article/:id',
    name: 'article',
    meta: { layout: 'default', auth: true },
    component: () => import('../views/Article.vue'),
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.auth)) {
    if (store.getters.GET_USER == '') {
      next({
       path: '/login',
        params: { nextUrl: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    if (store.getters.GET_USER != "") {
      next({
        path: '/',
        params: { nextUrl: '/' }
      })
    } else {
      next()
    }
  }
})

export default router
