import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'

//plagins
import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)

//Layout & Styles
import Default from "./layouts/Default.vue"
import Auth from "./layouts/Auth.vue"
Vue.component("default-layout", Default)
Vue.component("auth-layout", Auth)
import "./styles/index.scss"

//directives
Vue.directive('scroll', {
  inserted: function (el, binding) {
    let f = function (evt) {
      if (binding.value(evt, el)) {
        window.removeEventListener('scroll', f)
      }
    }
    window.addEventListener('scroll', f)
  }
})

Vue.filter('conversionDate', (value)=> {
  let date = new Date(value)
    let day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
    let month = (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : date.getMonth()
    let year = date.getFullYear() 
    return `${day}/${month}/${year}`
})



Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
